package g30021.dangi.daniel.l2.ex2b;

import java.util.Scanner;

public class Ex2b {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("number = ");
        int number = s.nextInt();

        String numberString;
        switch (number) {
            case 1:
                numberString = "ONE";
                break;
            case 2:
                numberString = "TWO";
                break;
            case 3:
                numberString = "THREE";
                break;
            case 4:
                numberString = "FOUR";
                break;
            case 5:
                numberString = "FIVE";
                break;
            case 6:
                numberString = "SIX";
                break;
            case 7:
                numberString = "SEVEN";
                break;
            case 8:
                numberString = "EIGHT";
                break;
            case 9:
                numberString = "NINE";
                break;
            case 0:
                numberString = "ZERO";
                break;
            default:
                numberString = "OTHER";
                break;

        }
        System.out.println(numberString);
    }
}
