package g30021.dangi.daniel.l2.ex5;

import java.util.Random;

public class Ex5 {

    public static void main(String[] args) {
        int v[] = new int[10];
        Random rand = new Random();
        for (int i = 0; i < v.length; i++) {
            v[i] = rand.nextInt(20);
            System.out.println(i + " = " + v[i]);
        }

        for (int j = 0; j < v.length; j++) {

            boolean swapped = false;
            int i = 0;
            while (i < v.length - 1) {

                if (v[i] > v[i + 1]) {

                    int temp = v[i];
                    v[i] = v[i + 1];
                    v[i + 1] = temp;

                    swapped = true;
                }
                i++;
            }

            if (!swapped) {
                break;
            }
        }

        for (int x : v) {
            System.out.println(x);
        }

    }
}
