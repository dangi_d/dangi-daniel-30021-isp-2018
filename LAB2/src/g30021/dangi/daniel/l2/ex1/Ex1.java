package g30021.dangi.daniel.l2.ex1;

import java.util.Scanner;

public class Ex1 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("1-st= ");
        int n1 = s.nextInt();
        System.out.println("2-nd= ");
        int n2 = s.nextInt();

        if (n1 > n2) {
            System.out.println("n1 este mai mare decat n2");
        } else if (n2 > n1) {
            System.out.println("n2 este mai mare decat n1");
        } else if (n1 == n2) {
            System.out.println("n1 = n2");
        }

    }
}
