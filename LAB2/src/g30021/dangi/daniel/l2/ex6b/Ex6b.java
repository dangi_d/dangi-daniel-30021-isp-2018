package g30021.dangi.daniel.l2.ex6b;

import java.util.Scanner;

public class Ex6b {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("N= ");
        int n = s.nextInt();

        System.out.println("N factorial = " + recursiv(n));
    }

    public static int recursiv(int n) {
        int p = 1;
        for (int i = 1; i <= n; i++) {
            p = p * i;
        }
        return p;
    }
}
