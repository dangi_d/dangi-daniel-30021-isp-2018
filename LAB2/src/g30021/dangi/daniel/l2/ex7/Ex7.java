package g30021.dangi.daniel.l2.ex7;

import java.util.Random;
import java.util.Scanner;

class Ex7 {

    public static void main(String args[]) {
        Random random = new Random();
        Scanner input = new Scanner(System.in);
        int MIN = 1;
        int MAX = 10;
        int aleator = random.nextInt(MAX);
        int nrmeu;
        int incercari = 0;
        do {
            System.out.print("Ghiceste numarul intre 1 si 10: ");
            nrmeu = input.nextInt();
            incercari++;

            if (nrmeu > aleator) {
                System.out.println("Nr generat e mai mic decat " + nrmeu);
            } else if (nrmeu < aleator) {
                System.out.println("Nr generat e mai mare decat " + nrmeu);
            } else {
                System.out.println("Well done! " + aleator + " este numarul , " + incercari + " incercari ");
            }
        } while (nrmeu != aleator && incercari < 3);
        System.out.println("You lose!");
    }
}
