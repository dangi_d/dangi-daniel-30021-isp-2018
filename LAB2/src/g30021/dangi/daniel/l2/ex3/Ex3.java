package g30021.dangi.daniel.l2.ex3;

import java.util.Scanner;

public class Ex3 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("A = ");
        int A = s.nextInt();

        System.out.println("B = ");
        int B = s.nextInt();

        int nr = 0;
        int i;

        for (i = A; i <= B; ++i) {
            if (i % 2 != 0) {
                nr++;
                System.out.println(i + " = nr prim ");
            }

        }
        System.out.println("Sunt " + nr + " numere prime ");
    }

}
