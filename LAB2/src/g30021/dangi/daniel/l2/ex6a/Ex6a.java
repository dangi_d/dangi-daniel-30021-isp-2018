package g30021.dangi.daniel.l2.ex6a;

import java.util.Scanner;

public class Ex6a {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("N= ");
        int n = s.nextInt();
        int p = 1;

        for (int i = 1; i <= n; i++) {
            p = p * i;
        }
        System.out.println("N factorial = " + p);
    }
}
