package g30021.dangi.daniel.l2.ex4;

import java.util.Scanner;

public class Ex4 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("nr de elem din vector = ");
        int n = s.nextInt();
        int max=0 ;
        int i;
        int v[] = new int[101];
        for (i = 1; i <= n; ++i) {
            System.out.println("Dati elementul " + i + " din " + n);
            v[i] = s.nextInt();
        }

        for (i = 1; i <= n; ++i) 
            {
            if (v[i] > max) 
                max = v[i];
            }

        
        System.out.println("Maximul din vector este " + max);
    }
}
