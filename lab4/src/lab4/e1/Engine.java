
package lab4.e1;

class Engine {
            void step(int x) {
                        start();
                        checkDirection(x);
                        execute(x);
                        stop();
            }
 
            private void start(){
                        System.out.println("Start engine.");
            }
            private void stop(){
                        System.out.println("Stop engine.");
            }
 
            private void checkDirection(int x){
                        if(x<0)
                                    System.out.println("Moving to the left.");
                        else if(x>0)
                                    System.out.println("Moving to the right.");
            }
 
            private void execute(int s){
                        System.out.println("Moving "+Math.abs(s)+" step(s).");
            }
 
 
  void Step(int y) {
    Start();
    CheckDirection(y);
    Execute(y);
    Stop();
}

            private void Start(){
    System.out.println("Start engine.");
}
            private void Stop(){
    System.out.println("Stop engine.");
}

            private void CheckDirection(int y){
    if(y<0)
                System.out.println("Moving down.");
    else if(y>0)
                System.out.println("Moving up.");
}

            private void Execute(int s1){
    System.out.println("Moving "+Math.abs(s1)+" step(s).");
}

}

