
package lab3.e3;


public class Flower {
    static int nr = 1;
    static int nrFlori = 21;
    
    Flower() { 
    	System.out.println("Floare adaugata \nSunt " + nr + " flori in gradina.\n");
    }

	public static void main(String[] args) {
		Flower[] gradina = new Flower[nrFlori];
		for(int i = 0; i < nrFlori; i++) {
			Flower f = new Flower();
			gradina[i] = f;
			nr++;
		}
		
		System.out.println("\nNumar de flori in gradina: " + nrFlori);
	}
}