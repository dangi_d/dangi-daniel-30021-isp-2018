
package lab3.e2;



public class Engine {

    String fuellType;
    long capacity;
    boolean active;
    double consum;

    Engine(int capacity, boolean active) {
        this.capacity = capacity;
        this.active = active;
    }

    Engine(int capacity, boolean active, String fuellType, double consum) {
        this(capacity, active);
        this.fuellType = fuellType;
        this.consum = consum;
    }

    Engine() {
        this(2000, false, "diesel", 5.5);
    }

    void print() {
        System.out.println("Engine: capacity = " + this.capacity + "  fuel = " + fuellType + "  active = " + active + "  consum = " + this.consum);
    }

    public static void main(String[] args) {
        Engine tdi = new Engine();
        Engine i16 = new Engine(1600, false, "petrol", 4.2);
        Engine d30 = new Engine(3000, true, "diesel", 9.2);
        tdi.print();
        i16.print();
        d30.print();
    }
}
