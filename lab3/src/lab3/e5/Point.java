package lab3.e5;

public class Point {

    int x;
    int y;

    Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void displayPoint() {
        System.out.println("\nPoint drawn.");
        System.out.println(" Point coordinates: " + x + ", " + y);
    }
}
