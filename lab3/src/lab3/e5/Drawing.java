
package lab3.e5;

public class Drawing {

    Point p = new Point(10, -23);
    Circle c = new Circle(1, -2, 5);
    Rectangle r = new Rectangle(0, 3, 4, 5);

    public void drawShapes() {
        p.displayPoint();
        c.displayCircle();
        r.displayRectangle();
    }

    public static void main(String[] args) {
        Drawing d = new Drawing();
        d.drawShapes();
    }
}
