package lab3.e5;

public class Circle {

    int x;
    int y;
    int radius;

    Circle(int x, int y, int radius) {
        this.x = x;
        this.y = y;
        this.radius = radius;
    }

    public void displayCircle() {
        System.out.println("\nCircle created. ");
        System.out.println(" Radius: R = " + radius);
        System.out.println(" Diameter: H = " + radius * 2);
        System.out.println(" Center point coordinates: " + x + ", " + y);
        System.out.println(" Area: A = " + radius * radius * 3.1418);
    }
}
