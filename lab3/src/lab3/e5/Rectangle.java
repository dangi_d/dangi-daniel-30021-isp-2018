package lab3.e5;

public class Rectangle {

    int x;
    int y;
    double side1; 
    double side2; 

    Rectangle(int x, int y, double side1, double side2) {
        this.x = x;
        this.y = y;
        this.side1 = side1;
        this.side2 = side2;
    }

    public void displayRectangle() {
        System.out.println("\nRectangle drawn. ");
        System.out.println(" Length: L = " + side1);
        System.out.println(" Height: H = " + side2);
        System.out.println(" Area: A = " + side1 * side2);
        System.out.println(" Center point at coordinates: " + x + ", " + y);
        System.out.println(" Vertices coordinates:");
        System.out.println("\tBottom Left point: " + (x - side1 / 2) + ", " + (y - side2 / 2));
        System.out.println("\tUpper Left point: " + (x - side1 / 2) + ", " + (y + side2 / 2));
        System.out.println("\tBottom Right point: " + (x + side1 / 2) + ", " + (y - side2 / 2));
        System.out.println("\tUpper Right point: " + (x + side1 / 2) + ", " + (y + side2 / 2));
    }
}
