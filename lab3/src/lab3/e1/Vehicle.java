/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab3.e1;

public class Vehicle {

    String color;
    String brand;
    int horsePower;

    void setHorsePower(int hp) {
        if (hp < 10 || hp > 800) {
            System.out.println("Valoare eronata, [raman aceiasi hp]");
        } else {
            horsePower = hp;
            System.out.println("HP=" + hp);
        }
    }

    void setColor(String c) {
        color = c;
    }

    void setBrand(String b) {
        brand = b;
    }

    void getBrand() {
        System.out.println(brand);
    }

    void getColor() {
        System.out.println(color);
    }

    int getHorsePower() {
        return horsePower;
    }

    public static void main(String[] args) {
        Vehicle v1 = new Vehicle();
        Vehicle v2 = new Vehicle();
        Vehicle v3 = new Vehicle();

        v1.setColor("Albastru");
        v2.setColor("Mov");
        v3.setColor("Rosu");

        v1.setBrand("BMW");
        v2.setBrand("Toyota");
        v3.setBrand("Volkswagen");

        v1.setHorsePower(163);
        v2.setHorsePower(210);
        v3.setHorsePower(140);

        System.out.println("\nCar 1: ");
        v1.getBrand();
        v1.getColor();
        System.out.println(v1.getHorsePower());

        System.out.println("\nCar 2: ");
        v2.getBrand();
        v2.getColor();
        System.out.println(v2.getHorsePower());

        System.out.println("\nCar 3: ");
        v3.getBrand();
        v3.getColor();
        System.out.println(v3.getHorsePower());
    }
}
