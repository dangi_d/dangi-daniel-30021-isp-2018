package lab3.e4;

public class CoffeTest {

    public static void main(String[] args) {
        CoffeMaker maker1 = new CoffeMaker();

        Coffee[] pachet = new Coffee[10];
        for (int i = 0; i < pachet.length; i++) {
            pachet[i] = maker1.getCofee();
        }

        for (int i = 0; i < pachet.length; i++) {
            pachet[i].drinkCofee();
        }
    }
}
