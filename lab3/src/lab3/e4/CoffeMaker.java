
package lab3.e4;

class CoffeMaker {
    CaffeineTank ctank = new CaffeineTank();
    WaterTank wtank = new WaterTank();
    CoffeTemp coffeeTemp = new CoffeTemp();
    
    CoffeMaker() {
    	System.out.println("New cofee maker created.");
    }
    
    Coffee getCofee() {
        int w = wtank.getIngredient();
        int c = ctank.getIngredient();
        int t = 100 - coffeeTemp.getTemp();
        return new Coffee(w, c, t);
    }
}
