
package lab3.e4;

class Coffee {
    int water;
    int caffeine;
    int temp;
    
    Coffee(int water, int caffeine, int temp) {
    	this.water = water; 
        this.caffeine = caffeine;
        this.temp = temp;
    }
    
    void drinkCofee() {
        System.out.println("Drink coffee [water = " + water + "  :  coffe = " + caffeine + "  :  temp = " + temp + "]");
    }
}